<?xml version="1.0"?>
<robot xmlns:xacro="http://www.ros.org/wiki/xacro">

    <xacro:property name="deck_x" value="0.2476"/>
    <xacro:property name="deck_y" value="0.2232"/>
    <xacro:property name="deck_z" value="0.004"/>
    <xacro:property name="deck_h" value="0.08"/>

    <xacro:property name="deck_pillar_l" value="0.08"/>
    <xacro:property name="deck_pillar_r" value="0.004"/>

    <xacro:property name="deck_pillar_border_x" value="0.02"/>
    <xacro:property name="deck_pillar_border_y" value="0.02"/>

    <xacro:property name="laser_base_x" value="0.0985"/>
    <xacro:property name="laser_base_y" value="0.070"/>
    <xacro:property name="laser_base_z" value="0.035"/>

    <xacro:property name="laser_l" value="0.025"/>
    <xacro:property name="laser_r" value="0.035"/>

    <xacro:property name="laser_margin_x" value="${laser_base_x / -2 + laser_r}"/>

    <!-- ### DECK PART LINKS & JOINTS ### -->

    <xacro:macro name="create_deck_part_link" params="name xParts yParts">
        <link name="${name}">
            <visual>
                <geometry>
                    <box size="${deck_x / xParts} ${deck_y / yParts} ${deck_z}"/>
                </geometry>
                <material name="lexan"/>
            </visual>
            <collision>
                <geometry>
                    <box size="${deck_x / xParts} ${deck_y / yParts} ${deck_z}"/>
                </geometry>
            </collision>
            <inertial>
                <mass value="0.1"/>
                <inertia ixx="0.001" ixy="0.0" ixz="0.0" iyy="0.001" iyz="0.0" izz="0.001"/>
            </inertial>
        </link>
    </xacro:macro>

    <xacro:create_deck_part_link name="deck_part_1" xParts="2" yParts="1"/>
    <xacro:create_deck_part_link name="deck_part_2" xParts="2" yParts="3"/>
    <xacro:create_deck_part_link name="deck_part_3" xParts="2" yParts="3"/>

    <xacro:macro name="create_deck_part_joint" params="name link x y">
        <joint name="${name}" type="fixed">
            <parent link="base"/>
            <child link="${link}"/>
            <origin xyz="${x} ${y} ${base_z / 2 + deck_h + deck_z / 2}"/>
        </joint>
    </xacro:macro>

    <xacro:create_deck_part_joint name="deck_part_1_joint" link="deck_part_1" x="${deck_x / -4}" y="0"/>
    <xacro:create_deck_part_joint name="deck_part_2_joint" link="deck_part_2" x="${deck_x / 4}" y="${deck_y / -3}"/>
    <xacro:create_deck_part_joint name="deck_part_3_joint" link="deck_part_3" x="${deck_x / 4}" y="${deck_y / 3}"/>

    <!-- ### DECK PILLAR LINKS & JOINTS ### -->

    <xacro:macro name="create_deck_pillar_link" params="name">
        <link name="${name}">
            <visual>
                <geometry>
                    <cylinder length="${deck_pillar_l}" radius="${deck_pillar_r}"/>
                </geometry>
                <material name="aluminium_grey"/>
            </visual>
            <collision>
                <geometry>
                    <cylinder length="${deck_pillar_l}" radius="${deck_pillar_r}"/>
                </geometry>
            </collision>
            <inertial>
                <mass value="0.01"/>
                <inertia ixx="0.0001" ixy="0.0" ixz="0.0" iyy="0.0001" iyz="0.0" izz="0.0001"/>
            </inertial>
        </link>
    </xacro:macro>

    <xacro:create_deck_pillar_link name="deck_pillar_1"/>
    <xacro:create_deck_pillar_link name="deck_pillar_2"/>
    <xacro:create_deck_pillar_link name="deck_pillar_3"/>
    <xacro:create_deck_pillar_link name="deck_pillar_4"/>

    <xacro:macro name="create_deck_pillar_joint" params="name link xSign ySign">
        <joint name="${name}" type="fixed">
            <parent link="base"/>
            <child link="${link}"/>
            <origin xyz="${xSign * (base_x / 2 - deck_pillar_border_x)} ${ySign * (base_y / 2 - deck_pillar_border_y)} ${base_z / 2 + deck_h / 2}"/>
        </joint>
    </xacro:macro>

    <xacro:create_deck_pillar_joint name="deck_pillar_1_joint" link="deck_pillar_1" xSign="1" ySign="1"/>
    <xacro:create_deck_pillar_joint name="deck_pillar_2_joint" link="deck_pillar_2" xSign="1" ySign="-1"/>
    <xacro:create_deck_pillar_joint name="deck_pillar_3_joint" link="deck_pillar_3" xSign="-1" ySign="-1"/>
    <xacro:create_deck_pillar_joint name="deck_pillar_4_joint" link="deck_pillar_4" xSign="-1" ySign="1"/>

    <!-- ### LASER LINKS & JOINTS ### -->

    <link name="laser_base">
        <visual>
            <geometry>
                <box size="${laser_base_x} ${laser_base_y} ${laser_base_z}"/>
            </geometry>
            <material name="plastic_fixed"/>
        </visual>
        <collision>
            <geometry>
                <box size="${laser_base_x} ${laser_base_y} ${laser_base_z}"/>
            </geometry>
        </collision>
        <inertial>
            <mass value="0.1"/>
            <inertia ixx="0.001" ixy="0.0" ixz="0.0" iyy="0.001" iyz="0.0" izz="0.001"/>
        </inertial>
    </link>

    <joint name="laser_base_joint" type="fixed">
        <parent link="deck_part_1"/>
        <child link="laser_base"/>
        <origin xyz="0 0 ${deck_z / 2 + laser_base_z / 2}"/>
    </joint>

    <link name="laser">
        <visual>
            <geometry>
                <cylinder length="${laser_l}" radius="${laser_r}"/>
            </geometry>
            <material name="plastic_sensor"/>
        </visual>
        <collision>
            <geometry>
                <cylinder length="${laser_l}" radius="${laser_r}"/>
            </geometry>
        </collision>
        <inertial>
            <mass value="0.1"/>
            <inertia ixx="0.001" ixy="0.0" ixz="0.0" iyy="0.001" iyz="0.0" izz="0.001"/>
        </inertial>
    </link>

    <joint name="laser_joint" type="fixed">
        <parent link="laser_base"/>
        <child link="laser"/>
        <origin xyz="${laser_margin_x} 0 ${laser_base_z / 2 + laser_l / 2}"/>
    </joint>

</robot>
