#!/usr/bin/env python
import rospy
import select
import sys
import termios
import tty
from geometry_msgs.msg import Twist

msg = """
A4WD1 Control
-------------

Moving around:
      z
 q    s    d

i/k : increase/decrease max speeds by 10%
o/l : increase/decrease only linear speed by 10%
p/m : increase/decrease only angular speed by 10%

CTRL-C to quit
"""

moveBindings = {
    'z': (1, 0),
    'q': (0, 1),
    'd': (0, -1),
    's': (-1, 0),
}

speedBindings = {
    'i': (1.1, 1.1),
    'k': (0.9, 0.9),
    'o': (1.1, 1.0),
    'l': (0.9, 1.0),
    'p': (1.0, 1.1),
    'm': (1.0, 0.9),
}


def getKey():
    tty.setraw(sys.stdin.fileno())
    rlist, _, _ = select.select([sys.stdin], [], [], 0.1)
    if rlist:
        key = sys.stdin.read(1)
    else:
        key = ''

    termios.tcsetattr(sys.stdin, termios.TCSADRAIN, settings)
    return key


def getSpeedsMsg(linear, angular):
    return "Currently:\tlinear %s\tangular %s" % (linear, angular)


if __name__ == "__main__":
    settings = termios.tcgetattr(sys.stdin)

    rospy.init_node('teleop')
    head = rospy.get_param('~head', False)
    topic = 'cmd_head' if head else 'cmd_vel'
    pub = rospy.Publisher(topic, Twist, queue_size=5)

    if head:
        linear = 0
        angular = 0.2
        max_linear = 0
        max_angular = 2.0
    else:
        linear = 0.2
        angular = 0.3
        max_linear = 1.0
        max_angular = 2.2

    x = 0
    z = 0

    try:
        print(msg)
        print(getSpeedsMsg(linear, angular))
        while 1:
            key = getKey()

            if key in moveBindings.keys():
                x = moveBindings[key][0]
                z = moveBindings[key][1]
            elif key in speedBindings.keys():
                linear = linear * speedBindings[key][0]
                angular = angular * speedBindings[key][1]

                linear = linear if linear < max_linear else max_linear
                angular = angular if angular < max_angular else max_angular

                print(getSpeedsMsg(linear, angular))
            elif key == ' ':
                x = 0
                z = 0
            else:
                x = 0
                z = 0
                if key == '\x03':
                    break
                else:
                    continue

            twist = Twist()
            twist.linear.x = 0 if head else linear * x
            twist.linear.y = 0
            twist.linear.z = 0
            twist.angular.x = angular * x if head else 0
            twist.angular.y = 0
            twist.angular.z = angular * z
            pub.publish(twist)

    except Exception as e:
        print(e)

    finally:
        twist = Twist()
        twist.linear.x = 0
        twist.linear.y = 0
        twist.linear.z = 0
        twist.angular.x = 0
        twist.angular.y = 0
        twist.angular.z = 0
        pub.publish(twist)

    termios.tcsetattr(sys.stdin, termios.TCSADRAIN, settings)
