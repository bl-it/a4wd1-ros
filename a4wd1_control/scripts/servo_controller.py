#!/usr/bin/env python
import rospy
from a4wd1_msgs.msg import Servo
from math import pi
from std_msgs.msg import Float64

servo_id = None
command_pub = None


def servo_cb(msg):
    if msg.servo_id == servo_id:
        float_msg = Float64()

        if msg.servo_id == 0:
            float_msg.data = pi * (90 - msg.value) / 180
        elif msg.servo_id == 1:
            float_msg.data = pi * (msg.value - 90) / 180

        command_pub.publish(float_msg)


if __name__ == '__main__':
    try:
        rospy.init_node("servo_controller", anonymous=True)
        servo_id = rospy.get_param("~servo_id")

        command_pub = rospy.Publisher("command", Float64, queue_size=1)
        rospy.Subscriber("cmd_servo", Servo, servo_cb, queue_size=1)

        rospy.spin()
    except rospy.ROSInterruptException:
        pass
