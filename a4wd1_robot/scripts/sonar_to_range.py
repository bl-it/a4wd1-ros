#!/usr/bin/env python
import rospy
from sensor_msgs.msg import Range
from std_msgs.msg import Float32, Header

frame_id = None
range_pub = None
range_msg = None


def init_range():
    global frame_id, range_msg

    range_msg = Range()
    range_msg.header = Header()
    range_msg.header.frame_id = frame_id
    range_msg.header.stamp = rospy.Time(0)
    range_msg.radiation_type = 0
    range_msg.field_of_view = 0.0872665
    range_msg.min_range = 0.01
    range_msg.max_range = 2.0


def sonar_raw_cb(msg):
    range_msg.range = msg.data if range_msg.min_range <= msg.data <= range_msg.max_range else 10.0
    range_pub.publish(range_msg)


if __name__ == '__main__':
    try:
        rospy.init_node("sonar_to_range")
        frame_id = rospy.get_param("~frame_id", "sonar")

        init_range()

        range_pub = rospy.Publisher("sonar", Range, queue_size=1)
        rospy.Subscriber("sonar_raw", Float32, sonar_raw_cb, queue_size=1)

        rospy.spin()
    except rospy.ROSInterruptException:
        pass
