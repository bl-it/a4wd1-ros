#!/usr/bin/env python
import rospy
from a4wd1_msgs.msg import Servo
from math import pi
from sensor_msgs.msg import JointState

js_pub = None

left_front_wheel_joint = 0
right_front_wheel_joint = 0
left_rear_wheel_joint = 0
right_rear_wheel_joint = 0
pth_servo_axis_joint = 0
ptv_servo_axis_joint = 0


def talker(data):
    js_msg = JointState()
    js_msg.header.stamp = rospy.get_rostime()

    js_msg.name = [
        "left_front_wheel_joint",
        "right_front_wheel_joint",
        "left_rear_wheel_joint",
        "right_rear_wheel_joint",
        "pth_servo_axis_joint",
        "ptv_servo_axis_joint"
    ]

    js_msg.position = [
        left_front_wheel_joint,
        right_front_wheel_joint,
        left_rear_wheel_joint,
        right_rear_wheel_joint,
        pth_servo_axis_joint,
        ptv_servo_axis_joint
    ]

    js_pub.publish(js_msg)


def servo_cb(msg):
    if msg.servo_id == 0:
        global pth_servo_axis_joint
        pth_servo_axis_joint = pi * (90 - msg.value) / 180
    elif msg.servo_id == 1:
        global ptv_servo_axis_joint
        ptv_servo_axis_joint = pi * (msg.value - 90) / 180


if __name__ == '__main__':
    try:
        rospy.init_node("robot_joint_state_publisher")
        publish_frequency = float(rospy.get_param("~publish_frequency", 10))

        js_pub = rospy.Publisher("joint_states", JointState, queue_size=1)
        timer = rospy.Timer(rospy.Duration(1 / publish_frequency), talker)

        rospy.Subscriber("servo", Servo, servo_cb, queue_size=1)

        rospy.spin()
        timer.shutdown()
    except rospy.ROSInterruptException:
        pass
