#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Polygon, Point32

ft_pub = None


def talker():
    point_1 = Point32()
    point_1.x = 0.18
    point_1.y = 0.20
    point_1.z = 0

    point_2 = Point32()
    point_2.x = -0.18
    point_2.y = 0.20
    point_2.z = 0

    point_3 = Point32()
    point_3.x = -0.18
    point_3.y = -0.20
    point_3.z = 0

    point_4 = Point32()
    point_4.x = 0.18
    point_4.y = -0.20
    point_4.z = 0

    footprint = Polygon()
    footprint.points = [point_1, point_2, point_3, point_4]

    ft_pub.publish(footprint)


if __name__ == '__main__':
    try:
        rospy.init_node('footprint_publisher', anonymous=True)
        ft_pub = rospy.Publisher('footprint', Polygon, queue_size=10)
        publish_frequency = float(rospy.get_param('~publish_frequency', 10))
        rate = rospy.Rate(publish_frequency)

        while not rospy.is_shutdown():
            talker()
            rate.sleep()
    except rospy.ROSInterruptException:
        pass
