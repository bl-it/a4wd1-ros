#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Point32
from sensor_msgs.msg import PointCloud, Range

pc_pub = None


def range_cb(msg):
    pc_msg = PointCloud()
    pc_msg.header = msg.header

    if msg.min_range <= msg.range <= msg.max_range:
        point = Point32()
        point.x = msg.range
        point.y = 0
        point.z = 0

        pc_msg.points = [point]

    pc_pub.publish(pc_msg)


if __name__ == '__main__':
    try:
        rospy.init_node("range_to_pointcloud", anonymous=True)
        pc_pub = rospy.Publisher("sonar_pc", PointCloud, queue_size=1)
        rospy.Subscriber("sonar", Range, range_cb, queue_size=1)

        rospy.spin()
    except rospy.ROSInterruptException:
        pass
